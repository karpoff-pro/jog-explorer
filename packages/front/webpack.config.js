const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');

const production = (process.env.NODE_ENV === 'production');

const config = {
  entry: {
    main: [
      'babel-polyfill',
      './src/index.js',
    ],
  },
  node: {
    net: 'empty',
    tls: 'empty',
    dns: 'empty',
  },
  output: {
    path: `${__dirname}/dist/`,
    filename: 'front/static/[name].js?[hash]',
    chunkFilename: 'front/static/[id].js',
    publicPath: '/',
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        resource: {
          test: /\.js$/,
          include: [
            path.resolve(__dirname, 'src'),
          ],
        },
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        resource: {
          test: /\.pug$/,
        },
        use: [
          {
            loader: 'pug-loader',
            options: { pretty: true },
          },
        ],
      },
      {
        resource: {
          test: /\.json$/,
          include: [
            path.resolve(__dirname, 'src'),
          ],
        },
        use: [
          {
            loader: 'json-loader',
          },
        ],
      },
      {
        resource: { test: /\.svg|\.woff|\.woff2|\.ttf|\.eot$/ },
        use: [
          { loader: 'file-loader' },
        ],
      },
    ],
  },
  resolve: {
    symlinks: false,
  },
  plugins: [
    new HtmlPlugin({
      title: 'jogme',
      filename: 'index.html',
      template: 'src/app/index.pug',
      inject: false,
    }),
  ],
  devtool: 'source-map',
};

if (production) {
  // do nothing for now
} else {
  config.devServer = {
    contentBase: false,
    publicPath: '/',
    port: 9001,
    host: 'localhost',
    historyApiFallback: {
      rewrites: [
        { from: /.*(?!api).*/, to: '/' },
      ],
    },
    proxy: {
      '/api': 'http://localhost:5000',
    },
  };
}

module.exports = config;
