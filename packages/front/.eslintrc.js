module.exports = {
  extends: [
    'airbnb',
    'plugin:flowtype/recommended',
  ],
  plugins: [
    'flowtype',
  ],
  parser: 'babel-eslint',
  env: {
    browser: true,
  },
  rules: {
    'consistent-return': 'off',
    'max-len': [
      'error',
      {
        code: 120,
        tabWidth: 2,
      },
    ],
    'no-param-reassign': 'off',
    'no-restricted-syntax': [
      'error',
      'LabeledStatement',
      'WithStatement',
    ],
    'no-use-before-define': 'off',
    'quote-props': [
      'error',
      'consistent',
    ],
    'import/extensions': 'off', // Temporary turned off.
    'import/no-unresolved': [
      'error',
    ],
    'react/jsx-curly-spacing': [
      'error',
      {
        when: 'always',
        attributes: true,
        children: true,
        allowMultiline: true,
      },
    ],
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: [
          '.js',
        ],
      },
    ],
  },
};
