import axios from 'axios';
import {actionShowError} from "../actions/info";

let token = null;
let apiUrl = (url) => `/api${url}`;


class ApiService {
    constructor() {
        token = window.localStorage['apiToken'];
    }

    async login(login, password) {
        return this.call({
            method: 'post',
            url: '/auth/login',
            params: {login, password},
        }).then(data => {
            token = data;
            window.localStorage['apiToken'] = token;
            return data;
        })
    }

    async logout() {
        delete window.localStorage['apiToken'];
        token = null;
    }

    call({url, method = 'get', params = null}) {
        const opts = {headers: {'x-access-token': token}};
        let prom;

        if (method === 'post') {
            prom = axios.post(apiUrl(url), params, opts);
        } else if (method === 'delete') {
            prom = axios.delete(apiUrl(url), opts);
        } else if (method === 'put') {
            prom = axios.put(apiUrl(url), params, opts);
        } else {
            prom = axios.get(apiUrl(url), opts);
        }

        return prom
            .then(res => {
                try {
                    return res.json();
                } catch (e) {
                    try {
                        return res.data;
                    } catch (e) {
                        return null;
                    }
                }
            })
            .catch(error => {
                throw error.response ? error.response.data : null;
            });
    }
}

export default new ApiService();
