import React from 'react';
import { connect } from 'react-redux';

import {actionLogout} from '../../actions/auth';

class AuthLogout extends React.Component {

    componentWillMount() {
        this.props.actionLogout();
    }

    render() {
        return 'logout';
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
});

const mapDispatchToProps = {
    actionLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthLogout);
