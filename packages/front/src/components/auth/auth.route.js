import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import {actionMe} from '../../actions/auth';

const AuthRoute = ({pub, user, loading, actionMe, ...props}) => {

    if (pub) {
        return <Route { ...props } />;
    }

    if (user === null) {
        if (!loading) {
            actionMe();
        }
        return 'loading';
    }

    if (user === false) {
        return <Redirect to="/login"  />;
    }

    return <Route { ...props } />;
};

AuthRoute.propTypes = {
    component: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.func
    ])
};

const mapStateToProps = (state) => ({
    user: state.auth.user,
    loading: state.auth.loading,
});

const mapDispatchToProps = {
    actionMe,
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthRoute);
