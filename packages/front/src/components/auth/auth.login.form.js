import React from 'react';
import {RaisedButton} from 'material-ui';

import { reduxForm, Field } from 'redux-form'
import {
    TextField,
} from 'redux-form-material-ui'

class LoginForm extends React.Component {
    render() {
        const { handleSubmit, loading = false } = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Field required name="login" component={TextField} fullWidth floatingLabelText="login" />
                <Field required name="password" component={TextField} fullWidth floatingLabelText="password" type="password"/>

                <RaisedButton type="submit" disabled={loading}>
                    Submit
                </RaisedButton>
            </form>
        )
    }
}

// Decorate with redux-form
LoginForm = reduxForm({
    form: 'loginForm'
})(LoginForm);

export default LoginForm
