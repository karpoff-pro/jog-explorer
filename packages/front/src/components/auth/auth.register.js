import React from 'react';
import { connect } from 'react-redux';

import {actionRegister} from '../../actions/auth';

import Auth from './';
import RegisterForm from './auth.register.form';

class AuthRegister extends React.Component {
    handleSubmit = (data) => {
        this.props.actionRegister({
            login: data.login,
            name: data.name,
            password: data.password,
        });
    };

    render() {
        const {loading, errors} = this.props;
        return (
            <Auth onSubmit={this.handleSubmit} type="register">
                <RegisterForm loading={loading} errors={errors} onSubmit={this.handleSubmit} />
            </Auth>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: state.auth.registering,
    errors: state.auth.registerErrors,
});

const mapDispatchToProps = {
    actionRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthRegister);
