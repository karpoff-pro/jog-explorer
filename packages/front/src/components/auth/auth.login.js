import React from 'react';
import { connect } from 'react-redux';

import {actionLogin} from '../../actions/auth';

import Auth from './';
import LoginForm from './auth.login.form';

class AuthLogin extends React.Component {
    handleSubmit = (data) => {
        this.props.actionLogin({
            login: data.login,
            password: data.password,
        });
    };


    render() {
        const {loading, errors} = this.props;
        return (
            <Auth onSubmit={this.handleSubmit} type="login">
                <LoginForm loading={loading} errors={errors} onSubmit={this.handleSubmit} />
            </Auth>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: state.auth.logining,
    errors: state.auth.loginErrors,
});

const mapDispatchToProps = {
    actionLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthLogin);
