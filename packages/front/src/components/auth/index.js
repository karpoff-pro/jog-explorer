import React from 'react';
import {Link} from 'react-router-dom';

import {Card, CardText, CardHeader} from 'material-ui/Card';

class Auth extends React.Component {
    render() {
        const {children, type = 'login'} = this.props;

        return (
            <div style={{display: 'flex', justifyContent: 'center', flex: 1}}>
                <Card>
                    <div style={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>
                        <h3>{type === 'login' ? 'Login' : 'Register'}</h3>
                        <Link style={{marginRight: 20}} to={type === 'login' ? '/register' : '/login'}>{type === 'login' ? 'Register' : 'Login'}</Link>
                    </div>

                    <CardText>
                        {children}
                    </CardText>
                </Card>
            </div>
        );
    }
}

export default Auth;