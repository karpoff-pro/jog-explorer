import React from 'react';
import { confirmation } from 'redux-form-validators'
import {RaisedButton} from 'material-ui';

import { reduxForm, Field } from 'redux-form'
import {
    TextField,
} from 'redux-form-material-ui'

class RegisterForm extends React.Component {
    render() {
        const { handleSubmit, loading = false } = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Field required name="login" component={TextField} fullWidth floatingLabelText="login" />
                <Field required name="name" component={TextField} fullWidth floatingLabelText="name"/>
                <Field required name="password" component={TextField} fullWidth floatingLabelText="password" type="password"/>
                <Field required name="confirm" component={TextField} fullWidth floatingLabelText="confirm" type="password" validate={confirmation({ field: 'password', fieldLabel: 'Password' })}/>

                <RaisedButton type="submit" disabled={loading}>
                    Submit
                </RaisedButton>
            </form>
        )
    }
}

// Decorate with redux-form
RegisterForm = reduxForm({
    form: 'registerForm'
})(RegisterForm);

export default RegisterForm
