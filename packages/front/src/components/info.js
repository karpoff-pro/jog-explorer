import React from 'react';
import {connect} from 'react-redux';
import Snackbar from 'material-ui/Snackbar';

class InfoPanel extends React.PureComponent {

    handleRequestClose = () => {
    };

    render() {
        const {message, type, show} = this.props;

        return (
            <Snackbar
                open={!!show}
                message={message || ''}
                autoHideDuration={4000}
                onRequestClose={this.handleRequestClose}
            />

        );
    }
}

const mapStateToProps = (state) => ({
    type: state.info.type,
    message: state.info.text,
    show: state.info.show,});

export default connect(mapStateToProps)(InfoPanel);
