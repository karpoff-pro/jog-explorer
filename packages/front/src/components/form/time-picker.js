import React from 'react';
import 'rc-time-picker/assets/index.css';
import TimePicker from 'rc-time-picker';
import moment from 'moment';

export default ({input: { onChange, value }}) => (
    <TimePicker
        style={{ width: 100 }}
        showSecond={true}
        className="xxx"
        value={moment(value, "HH:mm:ss")}
        onChange={(val) => { console.log(value); onChange(val.format('HH:mm:ss')) }}
    />
)