import React from 'react';
import {RaisedButton} from 'material-ui';

import { reduxForm, Field } from 'redux-form'
import {
    TextField,
    DatePicker
} from 'redux-form-material-ui'

import TimePickerComponent from '../form/time-picker';

class JogEditForm extends React.Component {
    render() {
        const { handleSubmit, loading = false } = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Field required name="distance" component={TextField} type="number" floatingLabelText="Distance (km)" />
                <Field required name="date" component={DatePicker} floatingLabelText="Date" />
                <Field required name="duration" component={TimePickerComponent} floatingLabelText="Date" />

                <div>
                    <RaisedButton type="submit" disabled={loading}>
                        Submit
                    </RaisedButton>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'registerForm'
})(JogEditForm);

