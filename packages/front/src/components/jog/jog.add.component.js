import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import moment from 'moment';

import {actionJogAdd} from '../../actions/jog-add';
import JogEditForm from './jog.edit.form';


class JogAdd extends React.Component {

    handleSubmit = (data) => {
        this.props.dispatch(actionJogAdd({
            distance: data.distance,
            duration: data.duration,
            date: moment(data.date).format("YYYY-MM-DD"),
        }));
    };

    render() {
        return (
            <JogEditForm
                initialValues={{
                    date: new Date(),
                    duration: '00:00:00'
                }}
                onSubmit={this.handleSubmit}
                loading={this.props.loading}
            />
        );
    }
}

const mapStateToProps = (state) => ({
    loading: state.add.processing,
});

export default withRouter(connect(mapStateToProps)(JogAdd));
