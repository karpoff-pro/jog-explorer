import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import DataTables from 'material-ui-datatables';
import moment from 'moment';

import {actionJogWeekReport} from '../../actions/jog-report';


class JogWeekReport extends React.Component {

    columns = [
        {
            key: 'isoStartWeek',
            label: 'Period',
            render: (value) => {
                const startDate = moment(new Date(value));
                return startDate.format('D MMM YYYY') + ' - ' + startDate.add(7, 'days').format('D MMM YYYY');
            }
        }, {
            key: 'averageDistance',
            label: 'average Distance (km)',
        }, {
            key: 'averageSpeed',
            label: 'Average speed (km/h)',
        }
    ];

    componentWillMount() {
        this.props.dispatch(actionJogWeekReport());
    }

    render() {
        return (

            <DataTables
                height={'auto'}
                selectable={false}
                showRowHover={true}
                columns={this.columns}
                data={this.props.list}
                showCheckboxes={false}
                showFooterToolbar={false}
            />

        );
    }
}

const mapStateToProps = (state) => ({
    list: state.report.list,
});


export default withRouter(connect(mapStateToProps)(JogWeekReport));
