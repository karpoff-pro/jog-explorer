import React from 'react';
import {connect} from 'react-redux';
import {withRouter, Link} from 'react-router-dom';
import DataTables from 'material-ui-datatables';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';

import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import ActionEdit from 'material-ui/svg-icons/editor/mode-edit';
import FilterIcon from 'material-ui/svg-icons/content/filter-list';
import ClearIcon from 'material-ui/svg-icons/content/clear';

import {actionJogList} from '../../actions/jog-list';
import {actionJogDelete} from '../../actions/jog-delete';

class JogList extends React.Component {

    state = {
        filters: [
            {
                id: 'dateFrom',
                title: 'Date from',
                active: false,
            },
            {
                id: 'dateTo',
                title: 'Date to',
                active: false,
            }
        ]
    };
    columns = [
        {
            key: 'date',
            label: 'Date',
        }, {
            key: 'distance',
            label: 'Distance (km)',
        }, {
            key: 'duration',
            label: 'Duration',
            render: (value) => {
                try {
                    const v = value.split(':');
                    const o = [];

                    if (parseInt(v[0])) {
                        o.push(v[0] + 'h');
                    }

                    if (parseInt(v[1])) {
                        o.push(v[1] + 'm');
                    }

                    if (parseInt(v[2])) {
                        o.push(v[2] + 's');
                    }
                    return o.join(' ');
                } catch (e) {
                    return value;
                }
            }
        }, {
            key: 'average',
            label: 'Average speed (km/h)',
        }, {
            label: '',
            render: (v, item) => (
                <div>
                    <IconButton
                        touch={true}
                        containerElement={<Link to={`/edit/${item.id}`}/>}
                    >
                        <ActionEdit/>
                    </IconButton>
                    <IconButton touch={true} onClick={() => this.deleteItem(item.id)}>
                        <ActionDelete/>
                    </IconButton>
                </div>
            )
        },
    ];

    componentWillMount() {
        this.props.dispatch(actionJogList());
    }

    deleteItem(itemId) {
        if (confirm('are you sure?')) {
            this.props.dispatch(actionJogDelete(itemId));
        }
    }

    updateFilters() {
        const filters = {};
        this.state.filters.filter(it => it.active && it.value).forEach(filter => {
            filters[filter.id] = filter.value;
        });
        this.props.dispatch(actionJogList({filters}));
    }
    handleFilterAdd = (e, id) => {
        const filters = [];
        this.state.filters.forEach(filter => {
            filters.push({
                ...filter,
                ...(filter.id === id ? {active: true} : {})
            });
        });
        this.setState({filters});
    };

    handleFilterRemove = (id) => {
        const filters = [];
        this.state.filters.forEach(filter => {
            filters.push({
                ...filter,
                ...(filter.id === id ? {active: false, value: null} : {})
            });
        });
        this.setState({filters}, () => {
            this.updateFilters();
        });
    };

    handleFilterChange(id, value) {
        const filters = [];
        this.state.filters.forEach(filter => {
            filters.push({
                ...filter,
                ...(filter.id === id ? {value} : {})
            });
        });
        this.setState({filters}, () => {
            this.updateFilters();
        });
    }

    renderFilter(filter) {
        return (
            <div style={{position: 'relative'}} key={filter.id}>
                <IconButton style={{position: 'absolute', right: 0, zIndex: 2, top: 20}}
                            onClick={() => this.handleFilterRemove(filter.id)}><ClearIcon/></IconButton>
                <DatePicker
                    floatingLabelText={filter.title}
                    autoOk={true}
                    onChange={(n, value) => this.handleFilterChange(filter.id, value)}
                    value={filter.value}
                />
            </div>
        )
    }

    render() {
        return (
            <div>
                <div style={{display: 'flex', alignItems: 'baseline'}}>
                    <IconMenu
                        iconButtonElement={<IconButton><FilterIcon/></IconButton>}
                        anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                        targetOrigin={{horizontal: 'right', vertical: 'bottom'}}
                        onChange={this.handleFilterAdd}
                    >
                        {this.state.filters.filter(it => !it.active).map(it => (
                            <MenuItem primaryText={it.title} key={it.id} value={it.id}/>
                        ))}
                    </IconMenu>

                    {this.state.filters.filter(it => it.active).map(it => this.renderFilter(it))}
                </div>
                <DataTables
                    height={'auto'}
                    selectable={false}
                    showRowHover={true}
                    columns={this.columns}
                    data={this.props.list}
                    showCheckboxes={false}
                    showFooterToolbar={false}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.list.list,
    filters: state.list.filters,
});


export default withRouter(connect(mapStateToProps)(JogList));
