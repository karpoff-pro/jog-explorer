import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import moment from 'moment';

import {actionJogGet} from '../../actions/jog-get';
import {actionJogEdit} from '../../actions/jog-edit';
import JogEditForm from './jog.edit.form';


class JogAdd extends React.Component {

    componentWillMount() {
        this.props.dispatch(actionJogGet(this.props.itemId));
    }

    handleSubmit = (data) => {
        this.props.dispatch(actionJogEdit(this.props.itemId, {
            distance: data.distance,
            duration: data.duration,
            date: moment(data.date).format("YYYY-MM-DD"),
        }));
    };

    render() {
        const {data} = this.props;

        if (!data) {
            return 'loading';
        }

        return (
            <JogEditForm
                initialValues={data}
                onSubmit={this.handleSubmit}
                loading={this.props.loading}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    console.log(ownProps);

    return {
        itemId: ownProps.match.params.id,
        data: state.get.data,
    };
};

export default withRouter(connect(mapStateToProps)(JogAdd));
