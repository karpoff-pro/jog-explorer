import {reducer as reduxFormReducer} from 'redux-form'
import {routerReducer} from 'react-router-redux'

import infoReducer from './info';
import authReducer from './auth';

import jogAddReducer from './jog-add';
import jogListReducer from './jog-list';
import jogGetReducer from './jog-get';
import jogEditReducer from './jog-edit';
import jogReportReducer from './jog-report';


export default {
    form: reduxFormReducer,
    routing: routerReducer,

    info: infoReducer,
    auth: authReducer,

    add: jogAddReducer,
    list: jogListReducer,
    get: jogGetReducer,
    edit: jogEditReducer,
    report: jogReportReducer,
};
