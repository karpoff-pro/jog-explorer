import {
    actionSuccess, actionFail, actionStart,
} from '../actions/jog-get';
import {createReducer} from 'redux-act';

const initialState = {
    loading: false,
    data: null,
};

const startReducer = (state) => ({
    ...state,
    loading: true,
    data: null,
});

const successReducer = (state, data) => ({
    ...state,
    loading: false,
    data: {
        ...data,
        date: new Date(data.date),
    },
});

const failReducer = (state, errors) => ({
    ...state,
    errors,
    loading: false,
});

export default createReducer({
    [actionSuccess]: successReducer,
    [actionFail]: failReducer,
    [actionStart]: startReducer,
}, initialState);
