import {
    actionSuccess, actionFail, actionStart,
} from '../actions/jog-edit';
import {createReducer} from 'redux-act';

const initialState = {
    processing: false,
    errors: null,
};

const startReducer = (state) => ({
    ...state,
    processing: true,
});

const successReducer = (state) => ({
    ...state,
    processing: false,
});

const failReducer = (state, errors) => ({
    ...state,
    errors,
    loading: false,
});

export default createReducer({
    [actionSuccess]: successReducer,
    [actionFail]: failReducer,
    [actionStart]: startReducer,
}, initialState);
