import {
    actionAuthMeSuccess, actionAuthMeFail, actionAuthMeStart,
    actionAuthRegisterSuccess, actionAuthRegisterFail, actionAuthRegisterStart,
    actionAuthLoginSuccess, actionAuthLoginFail, actionAuthLoginStart,
    actionAuthLogoutSuccess,
} from '../actions/auth';
import {createReducer} from 'redux-act';

const initialState = {
    user: null,
    loading: false,
    registering: false,
    registerErrors: null,
    logining: false,
    loginErrors: null,
};

const meStartReducer = (state, user) => ({
    ...state,
    loading: true,
});

const meSuccessReducer = (state, user) => ({
    ...state,
    user,
    loading: false,
});

const meFailReducer = (state) => ({
    ...state,
    user: false,
    loading: false,
});

const registerStartReducer = (state, user) => ({
    ...state,
    registering: true,
    registerErrors: null,
});

const registerSuccessReducer = (state) => ({
    ...state,
    registering: false,
});

const registerFailReducer = (state, error) => ({
    ...state,
    registerErrors: error,
    registering: false,
});

const loginStartReducer = (state, user) => ({
    ...state,
    logining: true,
    loginErrors: null,
});

const loginSuccessReducer = (state, token) => ({
    ...state,
    logining: false,
});

const loginFailReducer = (state, error) => ({
    ...state,
    loginErrors: error,
    logining: false,
});

const logoutSuccessReducer = (state, token) => ({
    ...initialState,
});

export default createReducer({
    [actionAuthMeSuccess]: meSuccessReducer,
    [actionAuthMeFail]: meFailReducer,
    [actionAuthMeStart]: meStartReducer,
    [actionAuthRegisterSuccess]: registerSuccessReducer,
    [actionAuthRegisterFail]: registerFailReducer,
    [actionAuthRegisterStart]: registerStartReducer,
    [actionAuthLoginSuccess]: loginSuccessReducer,
    [actionAuthLoginFail]: loginFailReducer,
    [actionAuthLoginStart]: loginStartReducer,
    [actionAuthLogoutSuccess]: logoutSuccessReducer,
}, initialState);
