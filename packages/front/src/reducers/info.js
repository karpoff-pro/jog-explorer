import { actionShowInfo, actionHideInfo } from '../actions/info';
import { createReducer } from 'redux-act';

const initialState = {
  show: false,
  text: null,
  type: null,
};

const showReducer = (state, {text, type}) => ({
  ...state,
  show: true,
  text,
  type,
});

const hideReducer = (state) => ({
  ...state,
  show: false,
});

export default createReducer({
  [actionShowInfo]: showReducer,
  [actionHideInfo]: hideReducer,
}, initialState);
