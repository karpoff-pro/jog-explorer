import {
    actionSuccess, actionFail, actionStart,
} from '../actions/jog-list';
import {createReducer} from 'redux-act';

const initialState = {
    loading: false,
    errors: null,
    list: [],
};

const startReducer = (state) => ({
    ...state,
    loading: true,
});

const successReducer = (state, {list}) => ({
    ...state,
    loading: false,
    list,
});

const failReducer = (state, errors) => ({
    ...state,
    errors,
    loading: false,
});

export default createReducer({
    [actionSuccess]: successReducer,
    [actionFail]: failReducer,
    [actionStart]: startReducer,
}, initialState);
