/* eslint-disable import/no-named-as-default */
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import {ToolbarGroup} from 'material-ui/Toolbar';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const Logged = ({user, handleRedirect}) => (
    <IconMenu
        iconButtonElement={
            <IconButton><MoreVertIcon/></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
    >
        <MenuItem>{user ? user.name : ''}</MenuItem>
        <MenuItem onClick={() => handleRedirect('/logout')}>Logout</MenuItem>
    </IconMenu>
);

const MyNavLinks = ({handleRedirect}) => (
    <ToolbarGroup>
        <FlatButton label="Jogs" onClick={() => handleRedirect('/')}/>
        <FlatButton label="Add" onClick={() => handleRedirect('/add')}/>
        <FlatButton label="Report" onClick={() => handleRedirect('/report')}/>
    </ToolbarGroup>
);

class App extends React.Component {
    handleRedirect = (path) => this.props.history.push(path);

    render() {
        const {user} = this.props;

        if (!user) {
            return this.props.children;
        }
        return (
            <div>
                <AppBar
                    iconElementRight={<Logged user={user} handleRedirect={this.handleRedirect}/>}
                    iconElementLeft={<MyNavLinks handleRedirect={this.handleRedirect}/>}
                />
                {this.props.children}
            </div>

        );
    }
}

App.propTypes = {
    children: PropTypes.element
};
const mapStateToProps = (state) => ({
    user: state.auth.user,
});

export default connect(mapStateToProps)(withRouter(App));
