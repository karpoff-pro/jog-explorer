import React from 'react';
import {Provider} from 'react-redux';
import {MuiThemeProvider} from 'material-ui/styles';
import {
    Switch,
} from 'react-router-dom'
import {ConnectedRouter} from 'react-router-redux'
import injectTapEventPlugin from 'react-tap-event-plugin';

import {history}  from './store/configureStore';
import App from './app';
import InfoPanel from '../components/info';


import Route from '../components/auth/auth.route';
import AuthLogin from '../components/auth/auth.login';
import AuthRegister from '../components/auth/auth.register';
import AuthLogout from '../components/auth/auth.logout';
import NotFound from '../components/not-found';

import JogList from '../components/jog/jog.list.component';
import JogAdd from '../components/jog/jog.add.component';
import JogEdit from '../components/jog/jog.edit.component';
import JogReport from '../components/jog/jog.report.component';

injectTapEventPlugin();

export default (props) => (
    <Provider store={props.store}>
        <MuiThemeProvider>
            <ConnectedRouter history={history}>
                <div>
                    <Switch>
                        <Route pub path="/login" component={AuthLogin}/>
                        <Route pub path="/register" component={AuthRegister}/>
                        <Route pub path="/logout" component={AuthLogout}/>

                        <Route exact path="/" component={() => <App><JogList/></App>}/>
                        <Route path="/add" component={() => <App><JogAdd/></App>}/>
                        <Route path="/report" component={() => <App><JogReport/></App>}/>
                        <Route path="/edit/:id" component={() => <App><JogEdit/></App>}/>

                        <Route pub component={NotFound}/>
                    </Switch>
                    <InfoPanel/>
                </div>
            </ConnectedRouter>
        </MuiThemeProvider>
    </Provider>
);
