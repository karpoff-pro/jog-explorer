import {createStore, compose, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware} from 'react-router-redux'
import reducers from '../../reducers';

import {apiMiddleware} from "./middleware";

export const history = createHistory();
const routeMiddleware = routerMiddleware(history);

const logger = createLogger({
    collapsed: true,
});

function configureStoreProd(initialState) {
    return createStore(combineReducers(reducers), initialState, compose(
        applyMiddleware(thunk, routeMiddleware, logger, apiMiddleware)
        )
    );
}

function configureStoreDev(initialState) {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
    const store = createStore(combineReducers(reducers), initialState, composeEnhancers(
        applyMiddleware(thunk, routeMiddleware, logger, apiMiddleware)
        )
    );

    return store;
}

const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProd : configureStoreDev;

export default configureStoreProd;
