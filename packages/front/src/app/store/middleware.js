import api from '../../services/api';
import {actionShowError} from '../../actions/info';

export const apiMiddleware = store => next => action => {
    if (action.type !== 'apiClient') {
        return next(action);
    }

    const {endpoint, params = {}, noError = false, method = 'get'} = action;

    if (action.actionStart) {
        store.dispatch(action.actionStart({params}))
    }


    api.call({
        url: endpoint,
        method,
        params,
    })
        .then(payload => {
            if (action.actionSuccess) {
                store.dispatch(action.actionSuccess(payload))
            }
        })
        .catch(({message, name = null, data = null}) => {
            console.log(typeof(action.actionError), action.actionError);

            if (!noError) {
                store.dispatch(actionShowError(message));
            }

            if (action.actionError) {
                store.dispatch(action.actionError(data))
            }
        });
};