import { createAction } from 'redux-act';
export const actionShowInfo = createAction('[INFO] show');
export const actionHideInfo = createAction('[INFO] hide');

const show = (type, text, dispatch) => {
  dispatch(actionShowInfo({type, text}));

  setTimeout(() => {
    dispatch(actionHideInfo())
  }, 3000)
};


export const actionShowError = (error) => (dispatch) => {
  show('error', error, dispatch);
};

export const actionShowSuccess = (success) => (dispatch) => {
  show('success', success, dispatch);
};
