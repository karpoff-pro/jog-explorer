import {createAction} from 'redux-act';
import moment from 'moment';
import {api} from './';

export const actionSuccess = createAction('[Jog list] success');
export const actionFail = createAction('[Jog list] fail');
export const actionStart = createAction('[Jog list] start');

const listEndpoint = (data) => {
    const url = '/jog';
    const gets = [];

    if (data.filters) {
       Object.keys(data.filters).forEach(id => {
           gets.push(`${id}Filter=${moment(data.filters[id]).format("YYYY-MM-DD")}`);
       })
    }

    return url + (gets.length ? ('?' + gets.join('&')) : '');
};
export const actionJogList = (data = {}) => api({
    endpoint: listEndpoint(data),
    actionStart: actionStart,
    actionSuccess: actionSuccess,
    actionError: actionFail,
});