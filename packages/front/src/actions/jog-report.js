import {createAction} from 'redux-act';
import moment from 'moment';
import {api} from './';

export const actionSuccess = createAction('[Jog report] success');
export const actionFail = createAction('[Jog report] fail');
export const actionStart = createAction('[Jog report] start');

export const actionJogWeekReport = (data = {}) => api({
    endpoint: '/jog-report/week',
    actionStart: actionStart,
    actionSuccess: actionSuccess,
    actionError: actionFail,
});