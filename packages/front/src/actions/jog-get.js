import {createAction} from 'redux-act';
import {push} from 'react-router-redux'
import {api} from './';
import {actionShowSuccess} from "./info";

export const actionSuccess = createAction('[Jog get] success');
export const actionFail = createAction('[Jog get] fail');
export const actionStart = createAction('[Jog get] start');


export const actionJogGet = (id) => api({
    endpoint: `/jog/${id}`,
    actionStart: actionStart,
    actionSuccess: actionSuccess,
    actionError: actionFail,
});
