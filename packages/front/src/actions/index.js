export const api = (options = {}) => ({
    type: 'apiClient',
    ...options,
});

api.get = 'get';
api.post = 'post';
api.put = 'put';
api.delete = 'delete';