import {createAction} from 'redux-act';
import {push} from 'react-router-redux'
import {api} from './';
import {actionShowSuccess} from "./info";

export const actionSuccess = createAction('[Jog edit] success');
export const actionFail = createAction('[Jog edit] fail');
export const actionStart = createAction('[Jog edit] start');


export const actionJogEdit = (id, data) => api({
    endpoint: `/jog/${id}`,
    method: api.put,
    params: data,
    actionStart: actionStart,
    actionSuccess: () => (dispatch) => {
        dispatch(actionSuccess());
        dispatch(actionShowSuccess('Jog info is updated'));
        dispatch(push('/'));
    },
    actionError: actionFail,
});

