import {createAction} from 'redux-act';
import {push} from 'react-router-redux'
import {api} from './';
import {actionShowError, actionShowSuccess} from "./info";

import apiService from '../services/api';

export const actionAuthMeSuccess = createAction('[AUTH] me success');
export const actionAuthMeFail = createAction('[AUTH] me fail');
export const actionAuthMeStart = createAction('[AUTH] me start');


export const actionAuthRegisterSuccess = createAction('[AUTH] Register success');
export const actionAuthRegisterFail = createAction('[AUTH] Register fail');
export const actionAuthRegisterStart = createAction('[AUTH] Register start');


export const actionAuthLoginSuccess = createAction('[AUTH] Login success');
export const actionAuthLoginFail = createAction('[AUTH] Login fail');
export const actionAuthLoginStart = createAction('[AUTH] Login start');


export const actionAuthLogoutSuccess = createAction('[AUTH] Logout success');
export const actionAuthLogoutFail = createAction('[AUTH] Logout fail');
export const actionAuthLogoutStart = createAction('[AUTH] Logout start');


export const actionMe = () => api({
    endpoint: '/auth/me',
    noError: true,
    actionStart: actionAuthMeStart,
    actionSuccess: actionAuthMeSuccess,
    actionError: actionAuthMeFail,
});


export const actionRegister = (data) => api({
    endpoint: '/auth/register',
    method: api.post,
    params: data,
    actionStart: actionAuthRegisterStart,
    actionSuccess: (result) => (dispatch) => {
        dispatch(actionAuthRegisterSuccess(result));
        dispatch(actionShowSuccess('Registration success'));

        apiService.login(data.login, data.password)
            .then(() => {
                dispatch(push('/'));
            })
            .catch(() => {
                dispatch(push('/login'));
            });
    },
    actionError: actionAuthRegisterFail,
});


export const actionLogin = (data) => (dispatch) => {
    dispatch(actionAuthLoginStart());

    apiService.login(data.login, data.password)
        .then(() => {
            dispatch(actionAuthLoginSuccess());
            dispatch(push('/'));
        })
        .catch((error) => {
            dispatch(actionShowError(error.message));
            dispatch(actionAuthLoginFail(error));
        })
};


export const actionLogout = () => (dispatch) => {
    dispatch(actionAuthLogoutStart());
    apiService.logout()
        .then(() => {
            dispatch(actionAuthLogoutSuccess());
            dispatch(push('/login'));
        })
        .catch((error) => {
            dispatch(actionAuthLogoutFail(error));
        })
};
