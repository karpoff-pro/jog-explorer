import {createAction} from 'redux-act';
import {api} from './';
import {actionShowSuccess} from "./info";
import {actionJogList} from "./jog-list";

export const actionSuccess = createAction('[Jog delete] success');
export const actionFail = createAction('[Jog delete] fail');
export const actionStart = createAction('[Jog delete] start');


export const actionJogDelete = (id) => api({
    endpoint: `/jog/${id}`,
    method: api.delete,
    actionStart: actionStart,
    actionSuccess: () => (dispatch) => {
        dispatch(actionSuccess());
        dispatch(actionShowSuccess('Jog info is removed'));
        dispatch(actionJogList());
    },
    actionError: actionFail,
});
