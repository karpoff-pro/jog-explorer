import {createAction} from 'redux-act';
import {push} from 'react-router-redux'
import {api} from './';
import {actionShowSuccess} from "./info";

export const actionSuccess = createAction('[Jog add] success');
export const actionFail = createAction('[Jog add] fail');
export const actionStart = createAction('[Jog add] start');


export const actionJogAdd = (data) => api({
    endpoint: '/jog',
    method: api.post,
    params: data,
    actionStart: actionStart,
    actionSuccess: () => (dispatch) => {
        dispatch(actionSuccess());
        dispatch(actionShowSuccess('Jog info is added'));
        dispatch(push('/'));
    },
    actionError: actionFail,
});

