export interface IJogAddRequest {
    duration: string;
    distance: number;
    date: string;
}


export interface IJogResponse {
    id: string;
    duration: string;
    distance: number;
    date: string;
    average: number;
}

export interface IJogListResponse {
    list: IJogResponse[];
}
