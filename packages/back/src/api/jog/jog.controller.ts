
import {Get, Post, Delete, Put, Route, Body, Controller, Security, Request, Query} from 'tsoa';
import {User} from '../../models/user.model';
import {JogService} from "../../service/jog.service";
import {IJogAddRequest, IJogListResponse, IJogResponse} from "./jog.models";

@Route('jog')
@Security('jwt')
export class JogController extends Controller {
    @Get('/')
    public getList(@Request() user: User, @Query() dateFromFilter?: string, @Query() dateToFilter?: string): Promise<IJogListResponse> {
        return JogService.list(user, {dateFromFilter, dateToFilter});
    }

    @Get('/{id}')
    public getItem(@Request() user: User, id: string): Promise<IJogResponse> {
        return JogService.get(user, id);
    }

    @Post('/')
    public async addJog(@Request() user: User, @Body() data: IJogAddRequest): Promise<void> {
        return JogService.add(user, data);
    }

    @Delete('/{id}')
    public async removeJog(@Request() user: User, id: string): Promise<void> {
        return JogService.remove(user, id);
    }

    @Put('/{id}')
    public async updateJog(@Request() user: User, id: string, @Body() data: IJogAddRequest): Promise<void> {
        return JogService.update(user, id, data);
    }
}