/* tslint:disable */
import { Controller, ValidateParam, FieldErrors, ValidateError, TsoaRoute } from 'tsoa';
import { AuthController } from './auth/auth.controller';
import { JogController } from './jog/jog.controller';
import { JogReportController } from './jogReport/jogReport.controller';
import { expressAuthentication } from './auth/auth.middleware';
import { ApiUnauthorizedError, ApiValidationError } from './errors';

const models: TsoaRoute.Models = {
    "User": {
        "properties": {
            "id": { "dataType": "string", "required": true },
            "login": { "dataType": "string", "required": true },
            "name": { "dataType": "string", "required": true },
        },
    },
    "IUserCreateRequest": {
        "properties": {
            "login": { "dataType": "string", "required": true },
            "name": { "dataType": "string", "required": true },
            "password": { "dataType": "string", "required": true },
        },
    },
    "IJogResponse": {
        "properties": {
            "id": { "dataType": "string", "required": true },
            "duration": { "dataType": "string", "required": true },
            "distance": { "dataType": "double", "required": true },
            "date": { "dataType": "string", "required": true },
            "average": { "dataType": "double", "required": true },
        },
    },
    "IJogListResponse": {
        "properties": {
            "list": { "dataType": "array", "array": { "ref": "IJogResponse" }, "required": true },
        },
    },
    "IJogAddRequest": {
        "properties": {
            "duration": { "dataType": "string", "required": true },
            "distance": { "dataType": "double", "required": true },
            "date": { "dataType": "string", "required": true },
        },
    },
    "IJogWeekReport": {
        "properties": {
            "isoStartWeek": { "dataType": "string", "required": true },
            "averageSpeed": { "dataType": "double", "required": true },
            "averageDistance": { "dataType": "double", "required": true },
        },
    },
    "IJogWeekReportResponse": {
        "properties": {
            "list": { "dataType": "array", "array": { "ref": "IJogWeekReport" }, "required": true },
        },
    },
};

export function RegisterRoutes(app: any) {
    app.get('/api/auth/me',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new AuthController();


            const promise = controller.getUser.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.post('/api/auth/register',
        function(request: any, response: any, next: any) {
            const args = {
                requestBody: { "in": "body", "name": "requestBody", "required": true, "ref": "IUserCreateRequest" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new AuthController();


            const promise = controller.createUser.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.post('/api/auth/login',
        function(request: any, response: any, next: any) {
            const args = {
                login: { "in": "body-prop", "name": "login", "required": true, "dataType": "string" },
                password: { "in": "body-prop", "name": "password", "required": true, "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new AuthController();


            const promise = controller.login.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.get('/api/jog/',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
                dateFromFilter: { "in": "query", "name": "dateFromFilter", "dataType": "string" },
                dateToFilter: { "in": "query", "name": "dateToFilter", "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new JogController();


            const promise = controller.getList.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.get('/api/jog/:id',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
                id: { "in": "path", "name": "id", "required": true, "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new JogController();


            const promise = controller.getItem.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.post('/api/jog/',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
                data: { "in": "body", "name": "data", "required": true, "ref": "IJogAddRequest" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new JogController();


            const promise = controller.addJog.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.delete('/api/jog/:id',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
                id: { "in": "path", "name": "id", "required": true, "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new JogController();


            const promise = controller.removeJog.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.put('/api/jog/:id',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
                id: { "in": "path", "name": "id", "required": true, "dataType": "string" },
                data: { "in": "body", "name": "data", "required": true, "ref": "IJogAddRequest" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new JogController();


            const promise = controller.updateJog.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.get('/api/jog-report/week',
        authenticateMiddleware([{ "name": "jwt" }]),
        function(request: any, response: any, next: any) {
            const args = {
                user: { "in": "request", "name": "user", "required": true, "dataType": "object" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = new JogReportController();


            const promise = controller.getReport.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });

    function authenticateMiddleware(security: TsoaRoute.Security[] = []) {
        return (request: any, response: any, next: any) => {
            let responded = 0;
            let success = false;
            for (const secMethod of security) {
                expressAuthentication(request, secMethod.name).then((user: any) => {
                    // only need to respond once
                    if (!success) {
                        success = true;
                        responded++;
                        request['user'] = user;
                        next();
                    }
                })
                    .catch((error: any) => {
                        responded++;
                        if (responded == security.length && !success) {
                            next(new ApiUnauthorizedError());
                        }
                    })
            }
        }
    }

    function promiseHandler(controllerObj: any, promise: any, response: any, next: any) {
        return Promise.resolve(promise)
            .then((data: any) => {
                let statusCode;
                if (controllerObj instanceof Controller) {
                    const controller = controllerObj as Controller
                    const headers = controller.getHeaders();
                    Object.keys(headers).forEach((name: string) => {
                        response.set(name, headers[name]);
                    });

                    statusCode = controller.getStatus();
                }

                if (data) {
                    response.status(statusCode || 200).json(data);
                } else {
                    response.status(statusCode || 204).end();
                }
            })
            .catch((error: any) => next(error));
    }

    function getValidatedArgs(args: any, request: any): any[] {
        const fieldErrors: FieldErrors = {};
        const values = Object.keys(args).map((key) => {
            const name = args[key].name;
            switch (args[key].in) {
                case 'request':
                    if (key === 'user') {
                        return request['user'];
                    }
                    return request;
                case 'query':
                    return ValidateParam(args[key], request.query[name], models, name, fieldErrors);
                case 'path':
                    return ValidateParam(args[key], request.params[name], models, name, fieldErrors);
                case 'header':
                    return ValidateParam(args[key], request.header(name), models, name, fieldErrors);
                case 'body':
                    return ValidateParam(args[key], request.body, models, name, fieldErrors, name + '.');
                case 'body-prop':
                    return ValidateParam(args[key], request.body[name], models, name, fieldErrors, 'body.');
            }
        });
        if (Object.keys(fieldErrors).length > 0) {
            throw new ApiValidationError(fieldErrors);
        }
        return values;
    }
}