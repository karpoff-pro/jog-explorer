import {Application, Request, Response, NextFunction} from 'express';
import * as bodyParser from 'body-parser';

import {RegisterRoutes} from "./routes";
import {ApiError, ApiNotFoundError} from "./errors";

export const initApi = (app: Application) => {
    app.use('/api', bodyParser.json());

    app.use('/api', (req: Request, res: Response, next: NextFunction) => {
        console.log(`[${req.method}] ${req.path}`);
        next();
    });

    RegisterRoutes(app);

    app.use('/api', () => {
        throw new ApiNotFoundError();
    });

    app.use('/api', (err: Error | ApiError, req: Request, res: Response, next: NextFunction) => {
        const error: ApiError = 'statusCode' in err ? <ApiError>err : new ApiError(err);
        console.error(err);
        res.status(error.statusCode).json({
            name: error.name,
            message: error.message,
            ...(error.data ? {data: error.data} : {}),
        })
    });
};