export class ApiError extends Error {
    statusCode = 400;
    data: any = null;

    constructor(args?: string | Error) {
        super();
        if (args instanceof Error) {
            this.message = args.message;
        } else if (typeof args === 'string') {
            this.message = args;
        }

        this.name = this.constructor.name;
    }
}

export class ApiNotFoundError extends ApiError {
    constructor() {
        super('API method not found');
        this.statusCode = 404;
    }
}
export class ApiUnauthorizedError extends ApiError {
    constructor() {
        super('API unauthorized access');
        this.statusCode = 401;
    }
}
export class ApiValidationError extends ApiError {
    constructor(fieldErrors: any) {
        super('Request validation error');
        this.data = fieldErrors;
    }
}