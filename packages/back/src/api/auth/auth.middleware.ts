import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import {UserService} from "../../service/user.service";

export function expressAuthentication(request: express.Request, securityName: string): Promise<any> {
    if (securityName === 'jwt') {
        const token = request.headers['x-access-token'];

        return token ? UserService.verify(token.toString()): Promise.reject(null);
    }
    return Promise.reject(null);
}