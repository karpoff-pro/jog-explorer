
import {Get, Post, Route, Body, SuccessResponse, Controller, Security, BodyProp, Request} from 'tsoa';
import {User} from '../../models/user.model';
import {IUserCreateRequest} from "./auth.models";
import {UserService} from "../../service/user.service";

@Route('auth')
export class AuthController extends Controller {
    @Get('/me')
    @Security('jwt')
    public getUser(@Request() user: User): User {
        return user;
    }

    @SuccessResponse('201', 'Created') // Custom success response
    @Post('/register')
    public async createUser(@Body() requestBody: IUserCreateRequest): Promise<void> {
        await UserService.create(requestBody);
        this.setStatus(201);
    }

    @Post('/login')
    public async login(@BodyProp() login: string, @BodyProp() password: string): Promise<any> {
        return UserService.login(login, password);
    }
}