export interface IUserCreateRequest {
    login: string;
    name: string;
    password: string;
}