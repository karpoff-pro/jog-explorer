export interface IJogWeekReportResponse {
    list: IJogWeekReport[];
}

export interface IJogWeekReport {
    isoStartWeek: string;
    averageSpeed: number;
    averageDistance: number;
}