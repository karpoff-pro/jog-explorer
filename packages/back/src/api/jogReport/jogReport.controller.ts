
import {Get, Route, Controller, Security, Request} from 'tsoa';
import {User} from '../../models/user.model';
import {JogService} from "../../service/jog.service";
import {IJogWeekReportResponse} from "./jogReport.models";

@Route('jog-report')
@Security('jwt')
export class JogReportController extends Controller {
    @Get('/week')
    public getReport(@Request() user: User): Promise<IJogWeekReportResponse> {
        return JogService.weekReport(user);
    }
}