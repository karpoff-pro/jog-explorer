import * as express from 'express';
import * as mongoose from 'mongoose';
import * as path from 'path';

import config from './config';
import {initApi} from "./api/init";

// Create a new express application instance
const app: express.Application = express();
// The port the express app will listen on
const port: number = config.port;

mongoose.connect(config.dbConnection, (err) => {
    if (err) {
        console.log(err.message, config.dbConnection);
        console.log(err);
    }
    else {
        console.log('Connected to MongoDb');

        app.use('/front/static', express.static(path.resolve('../front/dist/front/static')));

        initApi(app);

        app.get('*', (req: express.Request, res: express.Response) => {
            res.sendFile(path.resolve('../front/dist/index.html'));
        });

        // Serve the application at the given port
        app.listen(port, () => {
            // Success callback
            console.log(`Listening at http://localhost:${port}/`);
        });
    }
});

