import * as _ from 'lodash';

const resolveConfig = (): {} => Object.assign(
    Object.create(null),
    resolveJsonConfig(),
    resolveEnvConfig(),
);

const resolveJsonConfig = (): {} => {
    const ENV = (process.env['JOG_ENV'] || 'dev');
    const config = Object.create(null);

    _.merge(config, require(`../config/default.json`));
    try {
        _.merge(config, require(`../config/${ENV}.json`));
    } catch (e) {
    }

    return config;
};

const resolveEnvConfig = (): {} => {
    const config = Object.create(null);

    const envPrefix = 'JOG';
    const separator = '__';

    Object.keys(process.env)
        .filter(key => key.substr(0, envPrefix.length + separator.length) === envPrefix + separator)
        .forEach((key: string) => {
            const path: string[] = key.split(separator);

            path.shift();

            let obj = config;

            while (path.length) {
                const elem = _.camelCase(path.shift());

                if (path.length) {
                    if (!(elem in obj)) {
                        obj[elem] = {};
                    }

                    obj = obj[elem];
                } else if (typeof obj[elem] !== 'object') {
                    obj[elem] = process.env[key];
                }
            }
        });

    return config;
};

interface IConfig {
    dbConnection: string;
    port: number;
    user: {
        secret: string;
        hash: string;
    }
}

const config: IConfig = <IConfig>resolveConfig();

export default config;
