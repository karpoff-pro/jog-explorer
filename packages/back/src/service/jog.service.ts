import * as moment from 'moment';
import * as lodash from 'lodash';

import {User} from "../models/user.model";
import {JogEntity, IJogModel} from "../enities/jog";

import {IJogAddRequest, IJogListResponse, IJogResponse} from "../api/jog/jog.models";
import {IJogWeekReport, IJogWeekReportResponse} from "../api/jogReport/jogReport.models";

import {ApiError} from "../api/errors";

interface IListOptions {
    dateFromFilter?: string;
    dateToFilter?: string;
}

const calcDuration = (duration: string) => {
    const d = duration.split(':');
    return parseInt(d[2]) + parseInt(d[1]) * 60 + parseInt(d[0]) * 3600;
};

const showDuration = (duration: number) => {
    return `${Math.floor(duration / 3600) % 24}:${Math.floor(duration / 60) % 60}:${duration % 60}`;
};

const calcAverage = (item: IJogModel): number => {
    return Math.round((item.distance / (item.duration / 3600)) * 100) / 100;
};

export class JogService {
    static async add(user: User, data: IJogAddRequest): Promise<null> {
        const dt = new Date(data.date + 'T00:00:00Z');

        const jog = new JogEntity({
            date: dt,
            distance: data.distance,
            duration: calcDuration(data.duration),
            user: user.id,
        });

        await jog.save();
        return null;
    }

    static async remove(user: User, id: string): Promise<null> {
        const item = await JogEntity.findById(id);

        if (item) {
            if (item.user.toString() !== user.id) {
                throw new ApiError('a-ta-ta');
            }

            await item.remove();
        }
        return null;
    }

    static async get(user: User, id: string): Promise<IJogResponse> {
        const item = await JogEntity.findById(id);

        if (item) {
            if (item.user.toString() !== user.id) {
                throw new ApiError('a-ta-ta');
            }

            return {
                id: item.id.toString(),
                date: moment(item.date).utc().format("YYYY-MM-DD"),
                duration: showDuration(item.duration),
                distance: item.distance,
                average: calcAverage(item),
            };
        }
        throw new ApiError('a-ta-ta');
    }

    static async update(user: User, id: string, data: IJogAddRequest): Promise<null> {
        const item = await JogEntity.findById(id);

        if (item) {
            if (item.user.toString() !== user.id) {
                throw new ApiError('a-ta-ta');
            }

            item.date = new Date(data.date + 'T00:00:00Z');
            item.distance = data.distance;
            item.duration = calcDuration(data.duration);
            await item.save();
            return null;
        }
        throw new ApiError('a-ta-ta');
    }

    static async list(user: User, opts: IListOptions = {}): Promise<IJogListResponse> {

        const conditions: { [s: string]: any } = {};
        conditions.user = user.id;

        if (opts.dateFromFilter) {
            if (!conditions.date) {
                conditions.date = {};
            }

            conditions.date.$gte = new Date(opts.dateFromFilter); // ToDo: handle timezone here correctly
        }

        if (opts.dateToFilter) {
            if (!conditions.date) {
                conditions.date = {};
            }

            conditions.date.$lte = new Date(opts.dateToFilter); // ToDo: handle timezone here correctly
        }
        const items = await JogEntity.find(conditions);

        return {
            list: items.map((item) => ({
                id: item.id.toString(),
                date: moment(item.date).utc().format("YYYY-MM-DD"),
                duration: showDuration(item.duration),
                distance: item.distance,
                average: calcAverage(item),
            }))
        };
    }

    static async weekReport(user: User): Promise<IJogWeekReportResponse> {
        const items = await JogEntity.find({user: user.id});
        const out: IJogWeekReport[] = [];

        // ToDo: handle timezones here correctly
        let results = lodash.groupBy(items, (result) => moment(result.date).startOf('isoWeek'));

        Object.keys(results).forEach(week => {
            const speeds = [];
            const distances = [];

            for (const item of results[week]) {
                speeds.push(calcAverage(item));
                distances.push(item.distance);
            }

            out.push({
                isoStartWeek: week,
                averageSpeed: speeds.reduce((prev, curr) => prev + curr, 0) / speeds.length,
                averageDistance: distances.reduce((prev, curr) => prev + curr, 0) / distances.length,
            })
        });

        return {
            list: out,
        };
    }
}