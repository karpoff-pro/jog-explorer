import * as jwt from 'jsonwebtoken';
import * as md5 from 'md5';

import {IUserCreateRequest} from "../api/auth/auth.models";
import {User} from "../models/user.model";
import {UserEntity} from "../enities/user";
import {ApiError} from "../api/errors";

import config from '../config';

const hash = (password: string) => md5(password + config.user.hash);

interface IDecodedToken {
    data: string;
}
export class UserService {
    static async create(data: IUserCreateRequest): Promise<null> {
        const user = await UserEntity.findOne({login: data.login.trim()});

        if (user) {
            throw new ApiError("Already exists");
        }

        await new UserEntity({
            login: data.login.trim(),
            name: data.name,
            hash: hash(data.password),
        }).save();
        return null;
    }
    static async login(login: string, password: string): Promise<string> {
        const user = await UserEntity.findOne({login: login.trim()});

        if (user && hash(password) === user.hash) {
            return jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (3600 * 24 * 7),
                data: user.login
            }, config.user.secret);
        }

        throw new ApiError("Invalid credentials");
    }

    static async get(login: string): Promise<User> {
        const user = await UserEntity.findOne({login: login.trim()});

        if (!user) {
            throw new ApiError("User no found");
        }

        return {
            id: user._id.toString(),
            login: user.login,
            name: user.name,
        }
    }

    static verify(token: string): Promise<User> {
        return new Promise((resolve, reject) => {
            const decoded = <IDecodedToken> jwt.verify(token, config.user.secret);

            UserService.get(decoded.data.toString())
                .then((user: User) => resolve(user))
                .catch((err) => reject(err));
        })
    }
}