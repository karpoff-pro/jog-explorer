import { Document, Schema, Model, model} from "mongoose";

export interface IJog {
    date: Date;
    duration: number;
    distance: number;
    user: string;
}

export interface IJogModel extends IJog, Document {
}

export const JogSchema: Schema = new Schema({
    date: Date,
    duration: Number,
    distance: Number,
    user: Schema.Types.ObjectId
});

export const JogEntity: Model<IJogModel> = model<IJogModel>("Jog", JogSchema);
