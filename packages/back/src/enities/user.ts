import { Document, Schema, Model, model} from "mongoose";

export interface IUser {
    login?: string;
    name?: string;
    hash?: string;
}

export interface IUserModel extends IUser, Document {
}

export const UserSchema: Schema = new Schema({
    createdAt: Date,
    login: String,
    name: String,
    hash: String,
});
UserSchema.pre("save", function(next) {
    let now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});

export const UserEntity: Model<IUserModel> = model<IUserModel>("User", UserSchema);
