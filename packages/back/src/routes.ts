import * as express from 'express';
import * as path from 'path';

const router: express.Router = express.Router();

router.get('*', (req: express.Request, res: express.Response) => {
    res.sendFile(path.resolve('../front/dist/index.html'));
});

// Export the express.Router() instance to be used by server.ts
export const routes: express.Router = router;